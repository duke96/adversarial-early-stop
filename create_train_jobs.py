def create_train_command(job="job_adv", num_exp=10):
    with open(f"exp_{job}.sh", "w") as f:
        f.write(f"#!/bin/bash\n\ncd {job}\n")
        for i in range(num_exp):
            f.write(f"qsub train{i}.pbs\n")


def create_train_job(job, i, walltime, ncpus, ngpus, mem, epoch, target_layer, num_iter, attack):
    with open(f"{job}/train{i}.pbs", "w") as f:
        f.write(f'#!/bin/bash\n#PBS -P TOP\n#PBS -l walltime={walltime}:00:00\n#PBS -l select=1:ncpus={ncpus}:ngpus={ngpus}:mem={mem}GB\n\nmodule load python/3.7.7 magma/2.5.3 openmpi-gcc/3.1.5 cuda/10.2.89\n\ncd "/project/RDS-FEI-DDLN-RW/duke/adversarial_early_stop"\n')
        f.write(f'python experiment.py --epochs {epoch} --target_layers {target_layer} --num_iter {num_iter} --attack {attack} --root_path /project/RDS-FEI-DDLN-RW/duke/adversarial_early_stop')


job = "job_adv"
# PBS config
walltimes = [12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12]
ncpus = 2
ngpus = 1
mem = 8 #GB

# Training config
num_exp = 8
num_iters = [5, 10]
target_layers = ["0 1", "2", "3", "4 5"]

i=0
for num_iter in num_iters:
    for target_layer in target_layers:
        epoch = 100
        walltime = 12
        attack = "pgd"

        create_train_job(job, i, walltime, ncpus, ngpus, mem, epoch, target_layer, num_iter, attack)
        i += 1

create_train_command(job=job, num_exp=num_exp)