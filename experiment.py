import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import datasets, transforms
import torch.optim as optim
import os
import torch.nn as nn
import argparse
from utils import fgsm, pgd_linf, attack_pgd, epoch, epoch_adversarial
from model import make_resnet18k

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
logger.info(f"Using {device}")

parser = argparse.ArgumentParser(description='Organize raw data')
parser.add_argument('--root_path', default=".", type=str, help='path to data folder')
parser.add_argument('--batch_size', default=128, type=int, help='batch size')
parser.add_argument('--lr', type=float, help='learning rate', default=0.01)
parser.add_argument('--num_workers', default=4, type=int, help='')
parser.add_argument('--num_class', default=10, type=int, help='')
parser.add_argument('--epochs', default=100, type=int, help='total epoch')
parser.add_argument('--split_epoch', default=0.2, type=float, help='epoch to split training')
parser.add_argument('--num_iter', default=10, type=int, help='number of pgd steps')
parser.add_argument('--attack', default="pgd", type=str, help='attack type')
parser.add_argument('--frozen_layer', default=1, type=int, help='layer to freeze')
parser.add_argument('--resume', default='', type=str, metavar='PATH', help='path to latest checkpoint (default: none)')
parser.add_argument('--start_epoch', default=1, type=int, metavar='PATH', help='where to start training')
parser.add_argument('--target_layers', nargs='+', type=int)
parser.add_argument('--lr-max', default=0.1, type=float)

args = parser.parse_args()
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
logger.info(f"using device {device}")

# Create cifar10 data
transform_train = transforms.Compose([
    transforms.RandomCrop(32, padding=4),
    transforms.RandomHorizontalFlip(),
    transforms.ToTensor(),
    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
])

transform_test = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
])
train = datasets.CIFAR10("./data", train=True, download=True, transform=transform_train)
test = datasets.CIFAR10("./data", train=False, download=True, transform=transform_test)
train_loader = DataLoader(train, batch_size=args.batch_size, shuffle=True)
test_loader = DataLoader(test, batch_size=args.batch_size, shuffle=False)

# Print training setting
logger.info(f"Train setting:\n epochs: {args.epochs}\n split epoch: {args.split_epoch}\n attack: {args.attack}\n"
            f"PGD iters: {args.num_iter}\n frozen layer: {args.frozen_layer}\n"
            f"result dir: {args.root_path}/result\n model dir: {args.root_path}/model")


# Conduct experiment
def refine_net(net, target_layers, reinit=True):
    layers = [0, 1, 2, 3, 4, 5]
    # Freeze layer
    for i, layer in enumerate(net.children()):
        if i not in target_layers:
            logger.info(f"Freeze layer {i}")
            for param in layer.parameters():
                param.requires_grad=False

  # Reinit layer
    if reinit:
        for layer in layers:
            if layer in target_layers:
                net.reinit_layers(layer)
    net = net.to(device)
    opt = optim.SGD(net.parameters(), lr=args.lr, momentum=0.9, weight_decay=5e-4)
    return net, opt


def save_checkpoint(state, filename='checkpoint.pth.tar'):
    """
    Save the training model
    """
    torch.save(state, filename)


def experiment_base(total_epoch=100):
    logger.info("Training base model")
    net = make_resnet18k().to(device)
    opt = optim.SGD(net.parameters(), lr=args.lr, momentum=0.9, weight_decay=5e-4)

    best_err = 1.0
    for t in range(total_epoch):
        test_err, test_loss = epoch(test_loader, net, opt)

        is_best = test_err < best_err
        best_err = min(test_err, best_err)

        if is_best:
            logger.info(f"Saving best checkpoint, test_err={best_err}")
            save_checkpoint({
                'epoch': t + 1,
                'state_dict': net.state_dict(),
                'best_err': best_err,
            }, filename=f"{args.root_path}/model/base_model.pt")
    logger.info("Done training")


def experiment1(frozen_layer=1, split_epoch=1, total_epoch=2):

    # Create new model
    net = make_resnet18k().to(device)
    opt = optim.SGD(net.parameters(), lr=args.lr, momentum=0.9, weight_decay=5e-4)

    # Train
    for t in range(total_epoch):
    # Train adversarial for 10 epochs
        if t < round(total_epoch*split_epoch):
            train_err, train_loss = epoch_adversarial(train_loader, net, pgd_linf, opt, num_iter=args.num_iter)
            test_err, test_loss = epoch(test_loader, net)
            test_err_adv, test_loss_adv = epoch_adversarial(test_loader, net, pgd_linf, opt=None, num_iter=args.num_iter)
        elif t == round(total_epoch*split_epoch):
            logger.info("Start refining net")
            net, opt = refine_net(net, frozen_layer)
        else:
    # Train clean for 40 epochs
            train_err, train_loss = epoch(train_loader, net, opt)
            test_err, test_loss = epoch(test_loader, net)
            test_err_adv, test_loss_adv = epoch_adversarial(test_loader, net, pgd_linf, opt=None, num_iter=args.num_iter)

        text = f"Epoch {t}: Train_err: {round(train_err,2)} Test_err: {round(test_err,2)} Test_err_adv: {round(test_err_adv,2)}\n"
        logger.info(text)
        with open(f"{args.root_path}/result/resnet_exp_freeze={args.frozen_layer}_attack={args.attack}_num_iter={args.num_iter}_split_epoch={split_epoch}.txt", "a") as f:
            f.write(text)

    logger.info("Save model")
    torch.save(net.state_dict(), f"{args.root_path}/model/resnet_exp_freeze={args.frozen_layer}_attack={args.attack}_num_iter={args.num_iter}_split_epoch={split_epoch}.pt")
    return net


def experiment2(target_layers, total_epoch, lr_max, num_iter):
    # Load model checkpoint
    net = make_resnet18k().to(device)
    checkpoint = torch.load(f"{args.root_path}/model/base_model.pt")
    net.load_state_dict(checkpoint['state_dict'])
    logger.info(f"=> loaded checkpoint at epoch: {checkpoint['epoch']}, test_err: {checkpoint['best_err']}")

    # Freeze all layers except targeted layer
    net, opt = refine_net(net, target_layers=target_layers, reinit=False)

    # Train adversarial
    for t in range(total_epoch):
        train_err, train_loss = epoch_adversarial(train_loader, net, attack_pgd, opt, t=t, total_epoch=total_epoch, lr_max=lr_max, attack_iters=num_iter)
        test_err_adv, test_loss_adv = epoch_adversarial(test_loader, net, attack_pgd, opt=None, t=t, total_epoch=total_epoch, lr_max=lr_max, attack_iters=num_iter)
        test_err, test_loss = epoch(test_loader, net)
        text = f"Epoch {t}: Train_err: {round(train_err, 2)} Test_err: {round(test_err, 2)} Test_err_adv: {round(test_err_adv, 2)}\n"
        logger.info(text)
        with open(f"{args.root_path}/result/exp2_target={args.target_layers}_attack={args.attack}_num_iter={args.num_iter}.txt", "a") as f:
            f.write(text)

    logger.info("Save model")
    save_checkpoint(net.state_dict(), f"{args.root_path}/model/exp2_target={args.target_layers}_attack={args.attack}_num_iter={args.num_iter}.pt")

    return net


net = experiment2(target_layers=args.target_layers, total_epoch=args.epochs, lr_max=args.lr_max, num_iter=args.num_iter)


